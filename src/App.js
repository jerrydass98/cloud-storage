import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.scss';
import Home from './views/home';
import Login from './views/login'
import Error from './views/Error';
import Profile from './views/profile';
import SignUp from './views/signup';
import View from './views/file';
import ResetPassword from './views/reset_password';
import { useEffect,useState } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import axios from 'axios';
import { url } from './config';
import { setUser } from './actions';
import { Navigate } from 'react-router-dom';

function App() {

  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const user = useSelector(state => state.user);

  useEffect(() => {
    let token = sessionStorage.getItem('token');
    let refToken = localStorage.getItem('token');
    async function fetchToken() {

      if (refToken) {
        try {
          const response = await axios.post(
            `${url.baseUrl}/refresh-token`, {},
            {
              headers: { 'authorization': `Bearer ${refToken}` },
            }
          );
          if (response && response.status === 200) {
            token = response.data.access_token;
            sessionStorage.setItem('token', response.data.access_token);
            sessionStorage.setItem('token_expiry', response.data.token_expires_at);
            //localStorage.setItem('token', response.data.refresh_token);
          }
        } catch (err) {
          localStorage.clear();
          sessionStorage.clear();
        }
      }

      const options = (token) ? { headers: {authorization: `Bearer ${token}`}} : {};

      const response = await axios.post(
        `${url.baseUrl}/token`,{}, options
      );

      if (response && response.status === 200) {
        if (response.data.access_token) {
          token = response.data.access_token;
          sessionStorage.setItem('token',response.data.access_token);
          delete response.data.access_token;
        }
        if (response.data.data) {
          dispatch(setUser(response.data.data));
        }
        setIsLoading(false);
      }
    }
    if ((isLoading && !user) || !token) {
      fetchToken();
    }
  });

  return (
    <>
      { !isLoading ? (
        <Router>
          <div className="App">
            <Routes>
              <Route path='/' element={ user ? <Home /> : <Navigate to='/login' /> } />
              <Route path='/view' element={ user ? <View /> : <Navigate to='/login' />} />
              <Route path='/profile' element={ user ? <Profile /> : <Navigate to='/login' />} />
              <Route path='/login' element={ !user ? <Login /> : <Navigate to='/' /> } />
              <Route path='/signup' element={ !user ? <SignUp /> : <Navigate to='/' /> } />
              <Route path='/reset-password' element={ !user ? <ResetPassword /> : <Navigate to='/' /> } />
              <Route path='*' element={<Error />} />
            </Routes>
          </div>
        </Router>) : (<div className='App'>Loading...</div>) }
    </>
  );
}

export default App;
