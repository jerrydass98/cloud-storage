import Card from './card';
import { useSelector } from 'react-redux';

export default function FileGrid(props) {

    const files = useSelector(state => state.files) || [];
    const folders = useSelector(state => state.folders) || [];

    return (
        <div id="file-grid">
            <Card changePath={props.changePath} data={{'name':'..'}}/>
            { folders.map((f) => {
                return <Card
                    changePath={props.changePath}
                    editData={props.editData}
                    deleteData={props.deleteData}
                    key={f.name}
                    data={f}
                />;
            })}
            { files.map((f) => {
                return <Card
                    changePath={props.changePath}
                    editData={props.editData}
                    deleteData={props.deleteData}
                    copyData={props.copyData}
                    cutData={props.cutData}
                    key={f.name}
                    data={f}
                />;
            })}
        </div>
    );
}