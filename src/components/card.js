import './../styles/card.scss';
import { useEffect, useState } from 'react';
const dir = require('./../res/icon_directory.png');
const image = require('./../res/icons8-image-96.png')
const video = require('./../res/icons8-video-96.png');
const pdf = require('./../res/icons8-pdf-96.png');
const file = require('./../res/icons8-file-96.png');

export default function Card({data, changePath, deleteData, editData, copyData, cutData}) {

    const [icon, setIcon] = useState(null);
    const [menu, setMenu] = useState(false);

    useEffect(() => {
        if (data.mime?.mime.includes('video')) {
            setIcon(video);
        } else if (data.mime?.mime.includes('image')) {
            setIcon(image);
        } else if (data.mime?.mime.includes('pdf')) {
            setIcon(pdf);
        } else if (data?.mime?.mime) {
            setIcon(file);
        } else if (!data.mime) {
            setIcon(dir);
        }
    },[]);

    function toggleMenu() {
        setMenu(!menu);
    }

    return (
        <article id='card'>
            <img  onClick={() => changePath(data)} src={icon} alt='' />
            <p  onClick={() => changePath(data)}>{data.name}</p>
            {data.name !== '..' ? (<>
                <button className='delete-btn' onClick={() => toggleMenu()}>
                    X
                </button>
            </>) : <></>}
            { menu ? (
            <>
                <div className='menu'>
                    { data.mime ? (<><button onClick={() => {toggleMenu();copyData(data);}}>Copy</button>
                    <button onClick={() => {toggleMenu();cutData(data);}}>Cut</button></>) : <></>}
                    <button onClick={() => {toggleMenu();editData(data);}}>Rename</button>
                    <button onClick={() => {toggleMenu();deleteData(data);}}>Delete</button>
                </div>
            </>) : <></> }
        </article>
    );
}