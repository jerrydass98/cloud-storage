import './../styles/signup.scss';
import { Link, Navigate } from 'react-router-dom';
import axios from 'axios';
import { url } from './../config';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setUser } from './../actions';

function Signup() {

    const dispatch = useDispatch();

    useEffect(() => {
        document.title = 'Sign Up';
    })

    function validateNumber(event) {
        if (isNaN(event.key)) {
            event.preventDefault();
        }
    }

    function setColor(event) {
        event.target.style.borderColor = '#9b59b6';

    }

    function validatePassword(event) {
        let el = event.target;
        let nextEl = el.parentNode.nextSibling?.children[1] ?? el.parentNode.previousSibling.children[1];
        if (el.value === '' || nextEl.value === '' || (el.value === nextEl.value)) {
            nextEl.style.borderColor = (nextEl.value === '') ? '#ccc' : '#9b59b6';
            el.style.borderColor = (el.value === '') ? '#ccc' : '#9b59b6';
        } else if (
            el.value !== nextEl.value ||
            !(/[a-z]+/).test(el.value) ||
            !(/[A-Z]+/).test(el.value) ||
            !(/[0-9]+/).test(el.value) ||
            !(/[$_@+]+/).test(el.value)
        ){
            nextEl.style.borderColor = '#ff0000';
            el.style.borderColor = '#ff0000';
            alert('Password should have minimum 8 characters, one number, one capital alphabet and one special character(+,@,_,$)');
        }
    }

    function signup(event) {
        event.preventDefault();
        const token = sessionStorage.getItem('token');
        axios.post(
            `${url.baseUrl}/auth/register`,
            {
                'name': event.target[0]['value'],
                'user_name': event.target[1]['value'],
                'email': event.target[2]['value'],
                'phone_no': event.target[3]['value'],
                'password': event.target[4]['value'],
            },
            {
                'headers': {
                    'authorization': `Bearer ${token}`,
                }
            }
        )
            .then(response => {
                if (response.status === 200) {
                    sessionStorage.setItem('token',response.data.data.access_token);
                    sessionStorage.setItem('token_expiry',response.data.data.token_expires_at);
                    localStorage.setItem('token',response.data.data.refresh_token);
                    delete response.data.data.access_token;
                    delete response.data.data.token_expires_at;
                    delete response.data.data.refresh_token;
                    dispatch(setUser(response.data.data));
                    <Navigate to='/' />
                }
            })
    }

    return (
        <div className='signup'>
            <div className='container'>
                <div className='title'>Sign Up</div>
                <form onSubmit={signup} autoComplete='off'>
                    <div className="user-details">
                        <div className="input-box">
                            <span className="details">Name</span>
                            <input type="text" required/>
                        </div>
                        <div className="input-box">
                            <span className="details">Username</span>
                            <input type="text" minLength='6' required/>
                        </div>
                        <div className="input-box">
                            <span className="details">Email</span>
                            <input type="email" required/>
                        </div>
                        <div className="input-box">
                            <span className="details">Contact no.</span>
                            <input type="text" onKeyPress={validateNumber} maxLength='10' required/>
                        </div>
                        <div className="input-box">
                            <span className="details">Password</span>
                            <input type="password" onBlur={validatePassword} onFocus={setColor} minLength='8' required/>
                        </div>
                        <div className="input-box">
                            <span className="details">Confirm Password</span>
                            <input type="password" onBlur={validatePassword} onFocus={setColor} minLength='8' required/>
                        </div>
                    </div>
                    <div className="button">
                        <input type="submit" value='Sign Up' />
                    </div>
                    <div className='signup_link'>
                        Already a member? <Link to='/login'>Sign in</Link>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Signup;