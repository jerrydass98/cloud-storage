import './../styles/login.scss';
import { useEffect, useState } from 'react';
import { Navigate, Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { url } from './../config';
import { setUser } from './../actions';

function LoginUser() {

    const dispatch = useDispatch();
    const [errmsg, setErrMsg] = useState(null);

    useEffect(() => {
        document.title = 'Login';
    });

    function showErrorMsg(message) {
        setTimeout(() => setErrMsg(null),3000);
        setErrMsg(message);
    }

    function login(event) {
        event.preventDefault();
        const token = sessionStorage.getItem('token');
        axios.post(
            `${url.baseUrl}/auth/login`,
            {
                'password': event.target[1]['value'],
                'email': event.target[0]['value'],
            },
            {
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
            }
        )
            .then(response => {
                if (response.status === 200) {
                    sessionStorage.setItem('token',response.data.data.access_token);
                    localStorage.setItem('token',response.data.data.refresh_token);
                    sessionStorage.setItem('token_expiry', response.data.data.token_expires_at);
                    delete response.data.data.access_token;
                    dispatch(setUser(response.data.data));
                    return <Navigate to='/' />;
                }
            })
            .catch(err => {
                showErrorMsg(err.response.data.message);
            });
    }

    async function forgotPassword(event) {
        const email = prompt('Enter email id to receive reset link');
        if (email?.length>1) {
            const token = sessionStorage.getItem('token');
            axios.post(
                `${url.baseUrl}/auth/forgot-password`,
                {
                    'email': email,
                },
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            )
                .then(response => {
                    if (response.status === 200) {
                        alert('Email sent');
                    }
                })
                .catch(err => {
                    showErrorMsg(err.response.data.message);
                });
        }
    }

    return (
        <div className='login'>
            { errmsg ? (<label className='toast'>!{errmsg}</label>): null}
            <div>
                <h1>Login</h1>
                <form onSubmit={login} method='POST' autoComplete='off'>
                    <div className='txt_field'>
                        <input type='text' required/>
                        <span></span>
                        <label>Email / Username</label>
                    </div>
                    <div className='txt_field'>
                        <input type='password' required/>
                        <span></span>
                        <label>Password</label>
                    </div>
                    <article className='pass' onClick={() => forgotPassword()}>Forgot Password?</article>
                    <input type='submit' value='Login' />
                    <div className="signup_link">
                        Not a member? <Link to='/signup'>Sign Up</Link>
                    </div>
                </form>
            </div>
        </div>
    );
    
}

export default LoginUser;