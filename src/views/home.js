import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { url } from './../config';
import { setFolders, setFiles, selectFile, setPath } from './../actions';
import FileGrid from './../components/fileGrid';
import { useNavigate } from 'react-router-dom';
import './../styles/home.scss';

const Home = () => {

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [fetchData, setFetchData] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const user = useSelector(state => state.user);
    const path = useSelector(state => state.path);
    const [errmsg, setErrMsg] = useState(null);
    const [paste, setPaste] = useState(false);
    const [cutFile, setCutFile] = useState(null);
    const [copyFile, setCopyFile] = useState(null);

    useEffect(() => {
        document.title = 'Home';
    },[]);

    useEffect(() => {
        if (!fetchData && !isLoading) {
            getData();
        }
    })

    function showErrorMsg(message) {
        setTimeout(() => setErrMsg(null),3000);
        setErrMsg(message);
    }

    function getData() {
        setIsLoading(true);
        const token = sessionStorage.getItem('token');
        axios.get(`${url.baseUrl}/data`,
        {
            headers: {
                'authorization': `Bearer ${token}`,
            },
            params: {
                filter: {
                    path: path,
                },
            },
        })
            .then(response => {
                if (response.status === 200) {
                    dispatch(setFiles(response.data.data.files));
                    dispatch(setFolders(response.data.data.folders));
                    setFetchData(true);
                    setIsLoading(false);
                }
            })
            .catch(err => {
                showErrorMsg(err.response.data.message);
            });
    }

    function changePath(data) {
        if (data.mime) {
            dispatch(selectFile(data));
            return navigate('/view');
        } else {
            if (data.name === '..' && path.length > 1){
                data.name = path;
                data.name = data.name.split('/');
                data.name.pop();
                data.name = data.name.join('/');
            } else if (data.name !== path && path.length > 1) {
                data.name = (path === '/') ? path+data.name : path+'/'+data.name ;
            }
            if (data.name !== path && data.name !== '..') {
                if (data.name === '') {data.name = '/';}
                dispatch(setPath(data.name));
                setFetchData(false);
                setIsLoading(false);
            }
        }
    }

    function toggleHamburger() {
        const el = document.getElementsByClassName('controls')[0];
        if (el.style.left === '-300px') {
            el.style.left = '0px';
        } else {
            el.style.left = '-300px';
        }
    }

    function createFolderEvent() {
        const name = prompt('Enter directory name to create');
        if (name !== null && name.length > 0 && /^[a-zA-z]+.*/.test(name)) {
            const token = sessionStorage.getItem('token');
            axios.post(
                `${url.baseUrl}/data`,
                {
                    'name': name,
                    'type': 'folder',
                    'path': path,
                },
                {
                    headers: {
                        'authorization': `Bearer ${token}`,
                    }
                }
            )
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.data === true) {
                            setFetchData(false);
                            setIsLoading(false);
                        }
                    }
                })
                .catch(err => {
                    showErrorMsg(err.response.data.message);
                });
        }
        
    }

    function uploadFileEvent() {
        let input = document.createElement('input');
        input.type = 'file';
        input.setAttribute('name', 'files');
        input.setAttribute('mulitple','mulitple');
        input.onchange = _ => {
            const files = Array.from(input.files);
            if (files.length > 0) {
                const token = sessionStorage.getItem('token');
                const formData = new FormData();
                for(const file of files){
                    formData.append("file",file);
                }
                formData.append('type','file');
                formData.append('path',path);
                const el = document.querySelector('.progress');
                el.style.display = 'block';
                axios.post(
                    `${url.baseUrl}/data`,
                    formData,
                    {
                        headers: {
                            'authorization': `Bearer ${token}`,
                            'Content-Type': 'multipart/form-data',
                        },

                        onUploadProgress: (progressEvent) => {
                            const { loaded, total } = progressEvent;
                            let precentage = Math.floor((loaded * 100) / total);
                            el.style.width = (2*precentage)+'px';
                          }
                    }
                )
                    .then(response => {
                        if (response.status === 200) {
                            if (response.data.data === true) {
                                el.style.width = '0px';
                                el.style.display = 'none';
                                setFetchData(false);
                                setIsLoading(false);
                            }
                        }
                    }).catch(err => {
                        showErrorMsg(err.response.data.message);
                    });
            }
        };
        input.click();
    }

    function deleteData(data) {
        const type = (data.mime) ? 'file': 'folder';
        const text = (data.mime) ? `Confirm delete ${data.name}` : `Confirm delete ${data.name}\nAll data inside this folder will be deleted permanently.`;
        const result = window.confirm(text);
        if (result) {
            const token = sessionStorage.getItem('token');
            axios.delete(`${url.baseUrl}/data`,
            {
                headers: {
                    'authorization': `Bearer ${token}`,
                },
                params: {
                    filter: {
                        path: path,
                        name: data.name,
                        type: type,
                    },
                },
            })
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.data === true) {
                            setFetchData(false);
                            setIsLoading(false);
                        }
                    }
                })
                .catch(err => {
                    showErrorMsg(err.response.data.message);
                });
        }
    }

    function editData(data) {
        const name = prompt('Rename '+(data?.mime)?'file':'directory');
        if (name !== null && name.length > 0 && /^[a-zA-z]+.*/.test(name)) {
            const token = sessionStorage.getItem('token');
            axios.put(
                `${url.baseUrl}/data`,
                {
                    'newName': name,
                    'name': data.name,
                    'path': path,
                },
                {
                    headers: {
                        'authorization': `Bearer ${token}`,
                    }
                }
            )
                .then(response => {
                    if (response.status === 200) {
                        if (response.data.data === true) {
                            setFetchData(false);
                            setIsLoading(false);
                        }
                    }
                })
                .catch(err => {
                    showErrorMsg(err.response.data.message);
                });
        }
    }

    function pasteData() {
        const token = sessionStorage.getItem('token');
        let data = cutFile;
        let action = 'move';
        if (!data) {
            data = copyFile;
            action = 'copy';
        }
        axios.put(
            `${url.baseUrl}/data/${action}`,
            {
                'oldPath': data.path,
                'file': data.name,
                'newPath': path,
            },
            {
                headers: {
                    'authorization': `Bearer ${token}`,
                }
            }
        )
            .then(response => {
                if (response.status === 200) {
                    if (response.data.data === true) {
                        setPaste(false);
                        setFetchData(false);
                        setIsLoading(false);
                    }
                }
            })
            .catch(err => {
                showErrorMsg(err.response.data.message);
            });
    }

    function copyData(data) {
        setCutFile(null);
        setCopyFile({
            path: path,
            name: data.name,
        });
        setPaste(true);
    }

    function cutData(data) {
        setCopyFile(null);
        setCutFile({
            path: path,
            name: data.name,
        });
        setPaste(true);
    }

    return (
        <div className='home'>
            { errmsg ? (<label className='toast'>!{errmsg}</label>): null}
            <header>
                <button className="hamburger" onClick={() => toggleHamburger()}>
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <label>Cloud Storage</label>
                <div className="search-bar">
                    <input type="text" placeholder='Search...' />
                    <button><img src={require('./../res/search.png')} alt="" /></button>
                </div>
                <div className='profile'>
                    <label>{user.name.split(' ')[0]}</label>
                    <img src={require('./../res/user.png')} alt='' />
                </div>
            </header>
            <div className='container'>
                <div className='controls'>
                    <button onClick={() => createFolderEvent()}>Create Directory</button>
                    <button onClick={() => uploadFileEvent()}>Upload File</button>
                    <div className='progress'></div>
                    <button onClick={() => navigate('/profile')}>Account Setting</button>
                    { paste ? <button onClick={() => pasteData()}>Paste</button> : <></>}
                </div>
                { fetchData && <FileGrid editData={editData} cutData={cutData} copyData={copyData} deleteData={deleteData} changePath={changePath}/>}
            </div>
        </div>
    );
}

export default Home;