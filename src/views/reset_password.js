import axios from 'axios';
import { useEffect } from 'react';
import { url } from './../config';
import { useNavigate, useSearchParams } from 'react-router-dom';
import './../styles/reset.scss';

function ResetPassword () {

    const navigate = useNavigate();
    const [searchParams] = useSearchParams();

    useEffect(() => {
        document.title = 'Reset Password';
    });

    function resetPassword(event) {
        event.preventDefault();
        if (event.target[0].value !== event.target[1].value) {
            alert('Passwords don\'t match');
        } else {
            const token = sessionStorage.getItem('token');
            axios.post(
                `${url.baseUrl}/auth/reset-password`,
                {
                    'password_token': searchParams.get('password_token'),
                    'new_password': event.target[0].value,
                    'userid': searchParams.get('userid'),
                },
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            )
                .then(response => {
                    if (response.status === 200) {
                        navigate('/login');
                    }
                })
                .catch(err => {
                    alert(err.response.data.message);
                });
        }
    }

    return (
        <div className='reset-password' onSubmit={resetPassword}>
            <form autoComplete='off'>
                <div>
                    <label htmlFor="password">Enter new password</label>
                    <input type="password" name='password' required/>
                </div>
                <div>
                    <label htmlFor="password">Re-enter new password</label>
                    <input type="password" name='re-password' required/>
                </div>
                <input type='submit' value='Submit' />
            </form>
        </div>
    );
}

export default ResetPassword;