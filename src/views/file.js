import { useSelector } from 'react-redux';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';
import { url } from './../config';
import './../styles/file.scss';
import fileDownload from 'js-file-download'

export default function View() {

    const file = useSelector(state => state.selectedFile);
    const navigate = useNavigate();
    const path = useSelector(state => state.path);
    const [data, setData] = useState(null);

    useEffect(() => {
        if (!file) {
            return navigate('/');
        }
        document.cookie = `authorization=${sessionStorage.getItem('token')};`;
        document.title = file.name;
        if (file?.mime.mime.includes('image') || file?.mime.mime.includes('pdf') && data === null) {
            getData();
        }
        if (file?.mime.mime.includes('video') && data === null) {
            setInterval(async function() {
                const token = localStorage.getItem('token');
                try {
                    const response = await axios.post(`${url.baseUrl}/refresh-token`,{},{
                        headers: {
                            'authorization': `Bearer ${token}`,
                        }
                    });
                    sessionStorage.setItem('token', response.data.access_token);
                    sessionStorage.setItem('token_expiry', response.data.token_expires_at);
                    document.cookie = 'authorization='+response.data.access_token;
                } catch(err) {
                    if (err.response.data.message === 'Session expired') {
                        localStorage.clear();
                        sessionStorage.clear();
                    }
                }
            }, 260000);
        }
        //return () => { dispatch(selectFile(null));};
    },[]);

    function getData(download=false) {
        const token = sessionStorage.getItem('token');
        axios.get(
            `${url.baseUrl}/data/file`,
            {
                headers: {
                    'Content-Type': file.mime.mime,
                    'authorization': `Bearer ${token}`,
                },
                params: {
                    filter: {
                        path: path,
                        name: encodeURIComponent(file.name),
                    }
                },
                responseType: 'blob',
            }
        )
            .then(response => {
                if (response.status === 200) {
                    if (download) {
                        fileDownload(response.data, `${file.name}`);
                    } else {
                        setData(URL.createObjectURL(response.data));
                    }
                }
            });
    }

    return (
        <div className='file'>
            <div>
                <button onClick={() => navigate('/')}>Back</button>
                <p>{file?.name}</p>
            </div>
            { file?.mime.mime.includes('image') && data ? (<>
                <img className='data' src={data} alt=''/>
            </>) : null }
            { file?.mime.mime.includes('video') && !data ? (<>
                <video className='data' src={`${url.baseUrl}/stream?filter={"name":"${encodeURIComponent(file.name)}","path":"${path}"}`} controls></video>
            </>): null }
            { file?.mime?.mime.includes('pdf') && data ? window.open(data) : null }
            { !file?.mime.mime.includes('video') && !file?.mime.mime.includes('image') && !file?.mime.mime.includes('pdf')? (
            <>
                <button onClick={() => getData(true)}>Download</button>
            </>) : null }
        </div>
    );
}