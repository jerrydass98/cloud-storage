import axios from 'axios';
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { url } from './../config';
import { setUser } from './../actions';
import './../styles/profile.scss';

export default function Profile() {

    const navigate = useNavigate();
    const [user,setStateUser] = useState(useSelector(state => state.user));
    const dispatch = useDispatch();

    useEffect(() => {
        const form = document.querySelector('form');
        form.children[0].children[0].value = user.name;
        form.children[1].children[0].value = user.user_name;
        form.children[2].children[0].value = user.email;
        form.children[3].children[0].value = user.phone_no;
    },[user]);

    function validateNumber(event) {
        if (isNaN(event.key)) {
            event.preventDefault();
        }
    }

    function profileUpdate(event) {
        event.preventDefault();
        const token = sessionStorage.getItem('token');
        if (
            event.target[0]['value'] !== user.name ||
            event.target[1]['value'] !== user.user_name ||
            event.target[2]['value'] !== user.email ||
            event.target[3]['value'] != user.phone_no
        ) {
            axios.put(
                `${url.baseUrl}/auth/user/me`,
                {
                    'name': event.target[0]['value'],
                    'user_name': event.target[1]['value'],
                    'email': event.target[2]['value'],
                    'phone_no': event.target[3]['value'],
                },
                {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                    },
                }
            )
                .then(response => {
                    if (response.status === 200) {
                        sessionStorage.setItem('token',response.data.data.access_token);
                        localStorage.setItem('token',response.data.data.refresh_token);
                        sessionStorage.setItem('token_expiry', response.data.data.token_expires_at);
                        delete response.data.data.access_token;
                        dispatch(setUser(response.data.data));
                        setStateUser(response.data.data);
                    }
                });
        }
    }

    function logOut() {
        dispatch(setUser(null));
        sessionStorage.clear();
        localStorage.clear();
    }

    return (
        <div className='profile'>
            <header>
                <button className='back-btn' onClick={() => navigate('/')}>Back to home</button>
                <label>Account Settings</label>
                <button className='logout-btn' onClick={() => logOut()}>Logout</button>
            </header>
            <div className="container">
                <div className='basic-data'>
                    <h1>Personal Data</h1>
                    <form autoComplete='off' onSubmit={profileUpdate}>
                    <div className='txt_field'>
                        <input type='text' name='text' required/>
                        <span></span>
                        <label>Name</label>
                    </div>
                    <div className='txt_field'>
                        <input type='text' name='user_name' required/>
                        <span></span>
                        <label>Username</label>
                    </div>
                    <div className='txt_field'>
                        <input type='email' name='email' required/>
                        <span></span>
                        <label>Email</label>
                    </div>
                    <div className='txt_field'>
                        <input type='text' name='phone_no' maxLength='10' onKeyDown={validateNumber} required/>
                        <span></span>
                        <label>Contact no</label>
                    </div>
                    <input type="submit" value='Save' />
                </form>
                </div>
            </div>
        </div>
    );
}