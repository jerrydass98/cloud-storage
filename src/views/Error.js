import { Link } from 'react-router-dom';
import { useEffect } from 'react';

function Error() {

    useEffect(() => {
        document.title = 'Error 404';
    });

    return (
        <div className="notfound">
            <h2>Sorry</h2>
            <p>That page does not exist</p>
            <Link to="/">Back to home page</Link>
        </div>
    );
}

export default Error;