export function selectFile(value) {
    return {
        type: 'selectFile',
        payload: value,
    }
}

export function setUser(value) {
    return {
        type: 'loggedIn',
        payload: value,
    };
}

export function setFolders(value) {
    return {
        type: 'setFolders',
        payload: value,
    }
}

export function setFiles(value) {
    return {
        type: 'setFiles',
        payload: value,
    }
}

export function setPath(value) {
    return {
        type: 'setPath',
        payload: value,
    }
}