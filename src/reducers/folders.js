function folderReducer(state=[],action) {
    switch(action.type) {
        case 'setFolders': return action.payload;
        default: return state;
    }
}

export default folderReducer;