function selectedFileReducer(state=null,action) {
    switch(action.type) {
        case 'selectFile': return action.payload;
        default: return state;
    }
}

export default selectedFileReducer;