function pathReducer(state='/',action) {
    switch(action.type) {
        case 'setPath': return action.payload;
        default: return state;
    }
}

export default pathReducer;