function userReducer(state=null,action) {
    switch(action.type) {
        case 'loggedIn': return action.payload;
        case 'loggedOut': return null;
        default: return state;
    }
}

export default userReducer;