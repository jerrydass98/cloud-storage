import userReducer from './user';
import selectedFileReducer from './selectedFile';
import fileReducer from './files';
import folderReducer from './folders';
import pathReducer from './path';
import { combineReducers } from 'redux';

const rootReducers = combineReducers({
    user: userReducer,
    selectedFile: selectedFileReducer,
    files: fileReducer,
    folders: folderReducer,
    path: pathReducer,
});

export default rootReducers;