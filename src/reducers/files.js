function fileReducer(state=[],action) {
    switch(action.type) {
        case 'setFiles': return action.payload;
        default: return state;
    }
}

export default fileReducer;