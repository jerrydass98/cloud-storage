import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore } from 'redux';
import rootReducers from './reducers';
import { Provider } from 'react-redux'; 
import reportWebVitals from './reportWebVitals';
import axios from 'axios';
import { url } from './config';

axios.interceptors.request.use(async (request) => {
  const time = new Date().getTime();
  const expiry = sessionStorage.getItem('token_expiry');
  if (
    (time < expiry) ||
    [
      `${url.baseUrl}/refresh-token`,
      `${url.baseUrl}/token`,
      `${url.baseUrl}/auth/login`,
      `${url.baseUrl}/auth/register`,
      `${url.baseUrl}/auth/forgot-password`,
      `${url.baseUrl}/auth/reset-password`
    ].includes(request.url)
    ) {
    return request;
  } else if (time > expiry) {
    const token = localStorage.getItem('token');
    if (token) {
      try {
        const response = await axios.post(`${url.baseUrl}/refresh-token`,{},{
          headers: {
            'authorization': `Bearer ${token}`,
          }
        });
        sessionStorage.setItem('token', response.data.access_token);
        sessionStorage.setItem('token_expiry', response.data.token_expires_at);
        request.headers.authorization = `Bearer ${response.data.access_token}`;
        return request;
      } catch(err) {
        if (err.response.data.message === 'Session expired') {
          localStorage.clear();
          sessionStorage.clear();
        }
      }
    }
  }
});

const store = createStore(
  rootReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
